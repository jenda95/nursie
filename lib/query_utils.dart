import 'package:cloud_firestore/cloud_firestore.dart';

class QueryUtils {

  static Future<bool> doesInsuranceNumberExists(String insuranceNumber) async {
    final QuerySnapshot result = await FirebaseFirestore.instance.
    collection('patients').where('patient_card.insurance_number', isEqualTo: insuranceNumber).
    limit(1).get();
    final List<DocumentSnapshot> documents = result.docs;
    return documents.length == 1;
  }

  static Stream<QuerySnapshot<Map<String, dynamic>>> selectPatientByName(String patientName) {
    final Stream<QuerySnapshot<Map<String, dynamic>>> res = FirebaseFirestore.instance.
    collection('patients').where('patient_card.last_name', arrayContains: patientName).snapshots();
    return res;
  }

  static Future<QuerySnapshot<Map<String, dynamic>>> selectPatientByInsuranceNumber(String insuranceNumber) {
    final Stream<QuerySnapshot<Map<String, dynamic>>> res = FirebaseFirestore.instance.
    collection('patients').where('patient_card.insurance_number', isEqualTo: insuranceNumber).snapshots();
    Future<QuerySnapshot<Map<String, dynamic>>> result = res.first;
    return result;
  }

}