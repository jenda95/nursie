import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'query_utils.dart';

class FindPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FindPageState();
  }
}

class FindPageState extends State<FindPage> {
  TextEditingController _controller;

  bool changedSearchedText = false;
  String textOfSearchedPatient = '';
  // Future<List<DocumentSnapshot>> patientList;
  // ignore: close_sinks
  static StreamController<Stream<QuerySnapshot<Map<String, dynamic>>>> patientListController;
  // StreamController patientListController ;
  // Stream<QuerySnapshot<Map<String, dynamic>>> patientList = QueryUtils.selectPatientByName('');

  @override
  void initState() {
    patientListController = StreamController<Stream<QuerySnapshot<Map<String, dynamic>>>>();
    patientListController.add(QueryUtils.selectPatientByName(''));
    super.initState();
    _controller = TextEditingController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  // @override
  // Widget buildConnection(BuildContext context) {
  //   return StreamBuilder<MediaQuery>(
  //     stream: Firestore,
  //       builder: builder
  //   )
  // }
  // StreamBuilder _buildCOnnection(BuildContext context) {
  //
  // }

  @override
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> _getLocalFile(String filename) async {
    String dir = (await getApplicationDocumentsDirectory()).path;
    File f = new File('$dir/$filename');
    return f;
  }

  @override
  Stream<QuerySnapshot<Map<String, dynamic>>> _searchPatient(String patientName) {
    Stream<QuerySnapshot<Map<String, dynamic>>> result = QueryUtils.selectPatientByName(patientName);
    // numberOfPatientsFound = await result.asStream().length;
    return result;
  }


  @override
  Widget build(BuildContext context) {
    final path = _localPath;
    Stream<QuerySnapshot<Map<String, dynamic>>> patientListController;
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            child: new Image.asset(
              'assets/images/patient1.jpg',
              height: 70.0,
              fit: BoxFit.fitWidth,
              alignment: Alignment.topCenter,
            )
          ),
          Container(
            padding: EdgeInsets.only(top:20, left: 10, right: 10),
          // decoration: ,
          child: TextField(
            onChanged:  (text) {
              print('$text');
              textOfSearchedPatient = text;
              // patientList = this._searchPatient(text);
              // patientListController.add(this._searchPatient(text));

              // print(patientList.isEmpty);
            },
            controller: _controller,
            decoration: InputDecoration(
              hintText: 'Find patient',
            ),
            onSubmitted: (String value) async {
              await showDialog<void>(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text('Thanks!'),
                    content: Text(
                        'You typed "$value", which has length ${value.characters.length}.'),
                    actions: <Widget>[
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text('OK'),
                      ),
                    ],
                  );
                },
              );
            },
          ),
        ),
        Container(
          padding: EdgeInsets.only(top:20, left: 10, right: 10),
          // child: Text("test"),
          // child: StreamBuilder<QuerySnapshot<Map<String, dynamic>>>(
          //   stream: patientListController.stream,
          //   builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          //     return Text(textOfSearchedPatient);
          //     return ListView(
          //       scrollDirection: Axis.vertical,
          //       shrinkWrap: true,
          //       children: snapshot.data.docs.map((document) {
          //
          //         // return Center(
          //         //     child: Container(
          //         //         width: MediaQuery.of(context).size.width / 1.2,
          //         //         height: MediaQuery.of(context).size.height / 6,
          //         //         child: Text('neco')
          //         //     )
          //         // );
          //       }).toList(),
          //     );
          //     // if(snapshot.hasData) {
          //     //
          //     //   // return Text(snapshot.data.docs.first.data();
          //     //
          //     // }
          //     // return Text("it has no data");
          //   },
          // )
          // child: ListView.builder(
          //   scrollDirection: Axis.vertical,
          //   shrinkWrap: true,
          //   itemCount: 1,
          //   itemBuilder: (context, index) {
          //     return new Card(
          //             child: Text(
          //                 textOfSearchedPatient),
          //           );
          //   },
          // ),
          // child: ListView.builder(
          // itemBuilder: (context, index) {
          //   return new Card(
          //     child: Text('neco'),
          //   );
          // },
          //
          // )
          // child: Card(
          //   elevation: 5,
          //   // child: Text(patientList.asStream().elementAt(index).toString()),
          //   child: Text("neco"),
          // ),
          // child: ListView.builder(
          //   itemCount: 1,
          //   itemBuilder: (context, index) {
          //     return
          //       Container(
          //       padding: EdgeInsets.only(top:20, left: 10, right: 10),
          //       child: Card(
          //         elevation: 5,
          //         // child: Text(patientList.asStream().elementAt(index).toString()),
          //         child: Text("neco"),
          //       ),
          //     );
          //   },
          // ),
        ),
        ]
      ),
    );
  }
}