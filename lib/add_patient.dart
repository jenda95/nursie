import 'dart:io';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_app_test/query_utils.dart';
import 'package:uuid/uuid.dart';

import 'query_utils.dart';

class AddPatientPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AddPatientPageState();
  }
}

class AddPatientPageState extends State<AddPatientPage> {
  final _formKey = GlobalKey<FormState>();
  // final textEditController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final ageController = TextEditingController();
  final insuranceNumberController = TextEditingController();
  final commentController = TextEditingController();

  String infoMessage = '';
  bool patientExists = false;

  String firstName = '';
  String lastName = '';
  String age = '';
  String insuranceNumber = '';
  String comment = '';

  void _checkIfPatientExists() async {
    patientExists = await QueryUtils.doesInsuranceNumberExists(insuranceNumber);
    print(patientExists);
  }

  @override
  Future<bool> _persistData() async{
    print('First name: ${firstName}');
    print('Last name: ${lastName}');
    print('Age: ${age}');
    print('Insurance number: ${insuranceNumber}');
    print('Comment: ${comment}');
    print('persisting ...');

    patientExists = await QueryUtils.doesInsuranceNumberExists(insuranceNumber);
    print(patientExists);

    if(!patientExists) {
      Uuid uuid = Uuid();
      String uuidText = firstName.toLowerCase()+lastName.toLowerCase()+uuid.v1();
      FirebaseFirestore.instance
          .collection('patients').doc(uuidText).set({
        'patient_card': {
          'first_name': '${firstName}',
          'last_name' : '${lastName}',
          'age' : '${age}',
          'insurance_number' : '${insuranceNumber}',
          'comment' : '${comment}'
        }
      });
    } else {
    }
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.

    return Scaffold(
      body: Center(
        child: ListView(
          // shrinkWrap: true,
          children: [
          Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              child: Image.asset(
                'assets/images/new_patient2.jpg',
                height: 150.0,
                fit: BoxFit.fitWidth,
                alignment: Alignment.topCenter,
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10, left: 20, right: 20),
              child: Form(
                  key: _formKey,
                  child:
                  Column(
                      children: [
                        TextFormField(
                          decoration: InputDecoration(
                            labelText: 'First name',
                          ),
                          autofocus: true,
                          onChanged: (text) {
                            firstName = text;
                            print("First name TF: ${text}");
                          },
                          controller: firstNameController,
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                            labelText: 'Last name',
                          ),
                          onChanged: (text) {
                            lastName = text;
                            print("Last name TF: ${text}");
                          },
                          controller: lastNameController,
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                            labelText: 'Age',
                          ),
                          onChanged: (text) {
                            age = text;
                            print("Age TF: ${text}");
                          },
                          controller: ageController,
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                            labelText: 'Insurance number',
                          ),
                          onChanged: (text) {
                            insuranceNumber = text;
                            print("Insurance number TF: ${text}");
                          },
                          controller: insuranceNumberController,
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                            labelText: 'Comment',
                          ),
                          onChanged: (text) {
                            comment = text;
                            print("Comment TF: ${text}");
                          },
                          controller: commentController,
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10, right: 20),
                          child: IconButton(
                            iconSize: 50,
                            color: Colors.redAccent[200],
                            onPressed: () async {
                              await _checkIfPatientExists();
                              if(_formKey.currentState.validate()) {
                                // _persistData(_formKey.currentState.toString());
                                if(!patientExists) {
                                  _persistData();
                                  infoMessage = 'Adding patient ...';
                                  Future.delayed(const Duration(seconds: 2), (){
                                    firstNameController.clear();
                                    firstName = '';
                                    lastNameController.clear();
                                    lastName = '';
                                    ageController.clear();
                                    age = '';
                                    insuranceNumberController.clear();
                                    insuranceNumber = '';
                                    commentController.clear();
                                    comment = '';
                                  });
                                } else {
                                  infoMessage = 'Patient already exists!';
                                }
                                ScaffoldMessenger.of(context).showSnackBar(SnackBar(content:
                                  Text(infoMessage),));
                                // Navigator.pop(context, MaterialPageRoute(builder: (context) => HomePage()));
                              }
                            },
                            icon: Icon(Icons.add_circle),
                          )
                        )
                      ])
              ),
            ),
          ],
        ),
          ],
        ),
      )
    );
  }
}