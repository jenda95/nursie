import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import 'query_utils.dart';

class QRPage extends StatefulWidget {
  State<StatefulWidget> createState() => QRPageState();
}

class QRPageState extends State<QRPage> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode result;
  QRViewController controller;

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller.pauseCamera();
    } else if (Platform.isIOS) {
      controller.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
        MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 5,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
              overlay: QrScannerOverlayShape(
                  borderColor: Colors.red,
                  borderRadius: 10,
                  borderLength: 30,
                  borderWidth: 10,
                  cutOutSize: scanArea),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: StreamBuilder(
                stream: FirebaseFirestore.instance.
                collection('patients').where('patient_card.insurance_number', isEqualTo: result.code).snapshots(),
                builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  return ListView(
                    children: snapshot.data.docs.map((document) {
                      return Container(
                          child: Center(child: Card(
                            clipBehavior: Clip.antiAlias,
                            child: Column(
                              children: [
                                ListTile(
                                    leading:  Icon(Icons.arrow_drop_down_circle),
                                    title: Text(document['patient_card']['last_name'].toString()),
                                    subtitle: Text(document['patient_card']['first_name'].toString(),
                                        style: TextStyle(color: Colors.black.withOpacity(0.6)))
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Text(
                                    'age: '+document['patient_card']['age'].toString()+
                                        ', insurance number: '+document['patient_card']['insurance_number'].toString()+
                                        ', comment: '+document['patient_card']['comment'].toString(),
                                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                                  ),
                                ),
                              ],
                            ),
                            // Text(document['patient_card'].toString())
                          ),
                          )
                      );
                    }).toList(),
                  );
                },
              )
              // (result != null)
              //     ? QueryUtils.selectPatientByInsuranceNumber(result.code).
              //     Text(
              //     'Barcode Type: ${describeEnum(result.format)}   Data: ${result.code}')
              //     : Text('Scan a code'),
            ),
          )
        ],
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        result = scanData;
      });
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}