// External libraries
import 'package:flutter/material.dart';
import 'package:flutter_app_test/find_page.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

// Internal classes
import 'home_page.dart';
import 'qr_page.dart';
import 'find_page.dart';
import 'add_patient.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(Nursie());
}

class Nursie extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NursieState();
  }
}

class NursieState extends State<Nursie> {
  int _selectedTab = 0;
  final _pageOptions = [
    HomePage(),
    QRPage(),
    FindPage(),
    AddPatientPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          // primarySwatch: Colors.red,
          brightness: Brightness.light,
          primaryColor: Colors.redAccent[200],
          accentColor: Colors.redAccent[100],
          primaryTextTheme: TextTheme(
            title: TextStyle(color: Colors.white),
          )
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Nursie'),
        ),
        body: _pageOptions[_selectedTab],
        bottomNavigationBar:  BottomNavigationBar (
          type: BottomNavigationBarType.fixed,
          currentIndex: _selectedTab,
          onTap: (int index) {
            setState(() {
              _selectedTab = index;
            });
          },
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home'),
            ),
            BottomNavigationBarItem(
              // icon: Icon(Icons.add_a_photo_outlined),
              icon: Icon(Icons.add_a_photo_rounded),
              title: Text('Scan'),
            ),
            BottomNavigationBarItem(
              // icon: Icon(Icons.add_a_photo_outlined),
              icon: Icon(Icons.find_in_page),
              title: Text('Find'),
            ),
            BottomNavigationBarItem(
              // icon: Icon(Icons.add_a_photo_outlined),
              icon: Icon(Icons.accessibility_outlined),
              title: Text('Add'),
            ),
          ],
        ),
      )
    );
  }
}
