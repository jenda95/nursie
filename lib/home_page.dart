import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('patients').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return ListView(
            children: snapshot.data.docs.map((document) {
              return Container(
                child: Center(child: Card(
                  clipBehavior: Clip.antiAlias,
                    child: Column(
                      children: [
                        ListTile(
                          leading:  Icon(Icons.arrow_drop_down_circle),
                          title: Text(document['patient_card']['last_name'].toString()),
                          subtitle: Text(document['patient_card']['first_name'].toString(),
                          style: TextStyle(color: Colors.black.withOpacity(0.6)))
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Text(
                            'age: '+document['patient_card']['age'].toString()+
                                ', insurance number: '+document['patient_card']['insurance_number'].toString()+
                                ', comment: '+document['patient_card']['comment'].toString(),
                            style: TextStyle(color: Colors.black.withOpacity(0.6)),
                          ),
                        ),
                      ],
                    ),
                    // Text(document['patient_card'].toString())
                ),
                )
              );
            }).toList(),
          );
        },
      ),
    );
  }
  // Widget build(BuildContext context) {
  //   return Column(
  //     children: [
  //       Container(),
  //       Container()
  //     ],
  //   );
  // }
}